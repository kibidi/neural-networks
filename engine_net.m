%Kibidi Neocosmos
%Project
%Engine torque and emissions

clear all
close all
clc

%LOAD DATA SET
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[p,t]= engine_dataset;
[r,q]=size(p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NETWORK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%no. of neurons s1 and s2 in each layer
s1=10; s2=10;

%create network
net=feedforwardnet([s1,s2]);

%training,testing and validation sets
[ptrain,pval,ptest,trainInd,valInd,testInd]=dividerand(p,0.6,0.2,0.2);
[ttrain,tval,ttest]=divideind(t,trainInd,valInd,testInd);

%train
[net,netstruct]=train(net,p,t);

%name network and structure
net.userdata='engine';
Enet=net;
Estr=struct;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SIMULATE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
atrain=sim(Enet,ptrain); %activation for training set
atest=sim(Enet,ptest); %activation for test set
a=sim(Enet,p); %activation for all inputs


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ASSESSING DEGREE OF FIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%no. of elements in training and test set
q1=size(ptrain,2); %training set
q2=size(ptest,2); %test set

%training set
r2=rsq(ttrain,atrain);
[R,PV]=corrcoef(ttrain,atrain);

fprintf('Training:\n\n')
fprintf(' Correlation Coefficient: [%g, %g; %g, %g]\n p value: [%g, %g; %g, %g]\n r2: %g ,%g\n',R,PV,r2)
disp('-------------------------------------------------------------------------------')

figure
plot(ttrain,ttrain,ttrain,atrain,'*')
title(sprintf('Training with %g samples \n',q1))

%--------------------------------------------------------------------------------------

%test set
r2=rsq(ttest,atest);
[R,PV]=corrcoef(ttest,atest);

fprintf('Testing:\n\n')
fprintf(' Correlation Coefficient: [%g, %g; %g, %g]\n p value: [%g, %g; %g, %g]\n r2: %g ,%g\n',R,PV,r2)
disp('-------------------------------------------------------------------------------')

figure
plot(ttest,ttest,ttest,atest,'*')
title(sprintf('Testing with %g samples \n',q2))

%---------------------------------------------------------------------------------------

%all inputs
r2=rsq(t,a);
[R,PV]=corrcoef(t,a);

fprintf('Testing (all inputs):\n\n')
fprintf(' Correlation Coefficient: [%g, %g; %g, %g]\n p value: [%g, %g; %g, %g]\n r2: %g ,%g\n',R,PV,r2)
disp('-------------------------------------------------------------------------------')

figure
plot(t,t,t,a,'*')
title(sprintf('Testing (all inputs) with %g samples \n',q))

%---------------------------------------------------------------------------------------

%MATLAB regression plotting functions
%error histogram
figure
e=t-a;
ploterrhist(e,'bins',20)
%performance plot
figure
plotperform(netstruct)

%SAVE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
save engine.mat Enet