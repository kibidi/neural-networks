%Kibidi Neocosmos
%Exercise 5.1.3
%Question 5

%simulate given data x on neural network engine_net
function y=engine_fcn(x)

load engine.mat

y=sim(Enet,x);