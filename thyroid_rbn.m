%Kibidi Neocosmos
%Project
%Thyroid Classification

%thyroid_rbn

clear;clc;close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ORGANISE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input patterns and targets
[p,t]=thyroid_dataset;

%no. of centers
m=20;

%training index: choose m centers randomly
tri=randperm(7200);
tri=tri(1:m);

%test index
ti=setdiff([1:7200],tri);

%training set
ptrain=p(:,tri);
ttrain=t(:,tri);

%test set
ptest=p(:,ti);
ttest=t(:,ti);

%max dist and spread
d=max(max(dist(ptrain',ptrain)))
ss=d*sqrt(log(2))/sqrt(m)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NETWORK 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

S=linspace(ss-.3,ss+.3,11);
C=[];

for s=S
    %form the net
    net=newrb(ptrain,ttrain,0.3,s,m,1);
    
    %simulate
    atrain=sim(net,ptrain);
    atrain-ttrain;
    atest=sim(net,ptest);
    atest=round(atest);
    
    %assess network accuracy
    %correct classification on test set
    c=sum(all((atest-ttest)==0));
    
    %percentage correct
    pc=c/(7200-m)*100;
    
    C=[C; s c pc];
end
disp('       spread     correct    %correct')
C
best=max(C)
pc=best(3);

fprintf('percentage correct = %4.2f\n',pc)