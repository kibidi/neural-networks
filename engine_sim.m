%Kibidi Neocosmos
%Exercise 5.1.3
%Question 5

clear all
close all
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%GOAL:
%Determine relation between the inputs and outputs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%IMPORT DATA
load engine_dataset.mat
P=engineInputs';
T=engineTargets';
%minima
pm=min(P);
%maxima
pM=max(P);
%average
av=mean(P);

%VARY SPEED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%fix fuel rate
xS(1,:)=repmat(av(2),1,1200);
%speed
xS(2,:)=linspace(pm(1),pM(1),1200);

%VARY FUEL RATE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%fuel rate
xF(1,:)=linspace(pm(2),pM(2),1200);
%fix speed
xF(2,:)=repmat(av(1),1,1200);

%SIMULATE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
yS=engine_fcn(xS);
yF=engine_fcn(xF);

%PLOT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%varying speed
figure
plot(xS(2,:),yS(1,:))
title('SPEED VS TORQUE')
xlabel('speed')
ylabel('torque')

figure
plot(xS(2,:),yS(2,:))
title('SPEED VS NITROUS OXIDE EMISSIONS')
xlabel('speed')
ylabel('NO emissions')


%varying fuel rate
figure
plot(xF(1,:),yF(1,:))
title('FUEL RATE VS TORQUE')
xlabel('fuel rate')
ylabel('torque')

figure
plot(xF(1,:),yF(2,:))
title('FUEL RATE VS NITROUS OXIDE EMISSIONS')
xlabel('fuel rate')
ylabel('NO emissions')